# simple functions

function a() {
    Write-Host 'a() was called'
}

a

function b([Int]$argA, [Int]$argB) {
    Write-Host "b() was called"
    Write-Host "$argA + $argB = $($argA + $argB)"
}

b 1 3

function c() {
    Write-Host "c() was called"
    Write-Host "c() is calling d()"
    d
}

# this next line will fail because d is not declared yet
try {
    c
} catch {
    Write-Host 'c() threw an exception because d is not declared yet!'
}

function d() {
    Write-Host "d() was called"
}

c