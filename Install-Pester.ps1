# https://pester.dev/docs/introduction/installation

# remove the version that shipped with windows
# because it was signed by microsoft
$module = "C:\Program Files\WindowsPowerShell\Modules\Pester"

# https://en.wikipedia.org/wiki/Management_features_new_to_Windows_Vista#Command-line_tools
# takeown — Allows administrators to take ownership of a file for which access is denied. 
takeown /F $module /A /R 

# https://en.wikipedia.org/wiki/Cacls
<#
Displays or modifies access control lists (ACLs) and DACLs of files and directories.
It can also backup and restore them and set mandatory labels of an object for interaction
with Mandatory Integrity Control.
#>
icacls $module /reset
icacls $module /grant "*S-1-5-32-544:F" /inheritance:d /T
Remove-Item -Path $module -Recurse -Force -Confirm:$false

Install-Module -Name Pester -Force