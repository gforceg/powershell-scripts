
# install / update the latest NuGet provider
Install-PackageProvider -Name NuGet -Force
Exit

# install / update OneGet aka "PowerShellGet"
Install-Module -Name PowerShellGet -Force
Exit

Update-Module -Name PowerShellGet