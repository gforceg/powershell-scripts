# create the configuration
Configuration SmbShares
{
    Import-DscResource -ModuleName ComputerManagementDsc

    Node localhost
    {
        SmbShare 'TempShare'
        {
            Name = 'Temp'
            Path = 'C:\Temp'
        }
    }
}

# build the configuration
Write-Host "building configuration: SmbShares"
SmbShares

# test the dsc configuration
Write-Host "testing configuration: SmbShares"
Test-DscConfiguration .\SmbShares

# run the configuration
Write-Host "running configuration: SmbShares"
Start-DscConfiguration .\SmbShares -Wait

# test the dsc configuration
Write-Host "testing configuration: SmbShares"
Test-DscConfiguration .\SmbShares
